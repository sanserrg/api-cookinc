const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//inicialitzem mongoose
const db = require("mongoose");
db.Promise = global.Promise;

const conn_url = "mongodb://localhost:27017/cookinc_db";

//connectem a la bdd
db.connect(conn_url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log("Connexió OK!");
    })
    .catch(err => {
        console.log("Connexió falla...", err);
        process.exit();
    });

db.set('useFindAndModify', false);

const usuarioCollection = db.Schema(
    {
        nombre: String,
        email: String,
        password: String,
    }
);
const Usuario = db.model('usuario', usuarioCollection);

const recetaCollection = db.Schema(
    {
        nombre: String,
        fecha: String,
        foto: String,
        yummies: Number,
        tiempo: Number,
        categoria: String,
        id_perfil: String,
        ingredientes: [{
            cantidad: Number,
            unidad: String,
            nombre: String
        }],
        pasos: [{ paso: String }],
        comentarios: [{ comentario: String }]
    }
);
const Receta = db.model('receta', recetaCollection);


// definimos ruta de la api
app.get("/", (req, res) => {
    res.send("<h1>Benvingut a la API de Cook Inc!</h1>");
});

// consulta todos los usuarios
app.get("/api/usuarios", (req, res) => {
    Usuario.find()
        .then(data => {
            // hacemos cosas con los datos antes de enviar...
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// consulta todas las recetas
app.get("/api/recetas", (req, res) => {
    Receta.find()
        .then(data => {
            // hacemos cosas con los datos antes de enviar...
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// consulta un usuario por ID
app.get("/api/usuario/:id", (req, res) => {
    const id = req.params.id;
    Usuario.findById(id)
        .then(data => {
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// consulta una receta por ID
app.get("/api/receta/:id", (req, res) => {
    const id = req.params.id;
    Receta.findById(id)
        .then(data => {
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// crea un usuario
app.post("/api/registro", (req, res) => {
    if (!req.body.nombre && !req.body.password && !req.body.email) {
        res.status(400).send({ error: "No trobo l'usuari!" });
        return;
    }

    // usuario nuevo
    const usuario = new Usuario({
        nombre: req.body.nombre,
        email: req.body.email,
        password: req.body.password,
    });

    // lo guardamos en la bdd

    usuario.save(usuario)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// crea una receta
app.post("/api/:id_usuario/nueva", async (req, res) => {
    if (!req.body.nombre && !req.body.pasos && !req.body.ingredientes && !req.body.foto && !req.body.categoria) {
        res.status(400).send({ error: "Completa la recepta!" });
        return;
    }
    const id = req.params.id_usuario;

    await Usuario.findById(id, function (err, user) {
        if (err) {
            res.status(400).send({ error: err.message || "El ID no existe" });
            return;
        }
    })


    // receta nueva
    const receta = new Receta({

        nombre: req.body.nombre,
        fecha: req.body.fecha,
        foto: req.body.foto,
        yummies: 0,
        tiempo: req.body.tiempo,
        categoria: req.body.categoria,
        id_perfil: id,
        ingredientes: req.body.ingredientes,
        pasos: req.body.pasos,
        comentarios: req.body.comentarios

    });

    // la guardamos en la bdd

    receta.save(receta)
        .then(data => {
            res.send(data);

        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });

});

//elimina un usuario
app.delete("/api/usuario/:id", (req, res) => {
    const id = req.params.id;
    Usuario.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(500).send({ error: "No s'ha eliminat res" });
            } else {
                res.send({ msg: "Eliminat", id: id });
            }
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// elimina una receta
app.delete("/api/receta/:id", (req, res) => {
    const id = req.params.id;
    Receta.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(500).send({ error: "No s'ha eliminat res" });
            } else {
                res.send({ msg: "Eliminat", id: id });
            }
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// modifica un usuario
app.put("/api/usuario/:id", (req, res) => {
    if (!req.body) {
        res.status(400).send({ error: "No trobo dades!" });
        return;
    }

    const id = req.params.id;

    Usuario.findByIdAndUpdate(id, req.body)
        .then(data => {
            if (!data) {
                res.send({ error: "No s'ha actualitzat res" });
            } else {
                res.send({ msg: "Actualitzat" });
            }
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// modifica una receta
app.put("/api/receta/:id", (req, res) => {
    if (!req.body) {
        res.status(400).send({ error: "No trobo dades!" });
        return;
    }

    const id = req.params.id;

    Receta.findByIdAndUpdate(id, req.body)
        .then(data => {
            if (!data) {
                res.send({ error: "No s'ha actualitzat res" });
            } else {
                res.send({ msg: "Actualitzat" });
            }
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// set port, listen for requests
const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});



// // BATALLA!
// app.get("/api/batalla/:id1/:id2", async (req, res) => {
//     const id1 = req.params.id1;
//     const id2 = req.params.id2;
//     const robot1 = await Robot.findById(id1);
//     const robot2 = await Robot.findById(id2);

//     const combate = () => {
//         let ret = false;

//         const vida1 = robot1.caracteristicas.vida - (robot2.caracteristicas.ataque - robot1.caracteristicas.defensa);
//         const vida2 = robot2.caracteristicas.vida - (robot1.caracteristicas.ataque - robot2.caracteristicas.defensa);

//         if (vida1 > vida2) {
//             ret = true;
//         } else {
//             ret = false;
//         }

//         return ret;
//     }

//     const ganador = combate() ? robot1 : robot2;
//     res.status(200).json(ganador);

// });

// // consulta TOTES les batalles
// app.get("/api/partidas", (req, res) => {
//     Batalla.find()
//         .then(data => {
//             // hacemos cosas con los datos antes de enviar...
//             res.status(200).json(data);
//         })
//         .catch(err => {
//             res.status(500).send({
//                 error: err.message || "Alguna cosa falla."
//             });
//         });
// });

// app.post("/api/partidas", (req, res) => {
//     if (!req.body.id_ganador) {
//         res.status(400).send({ error: "No trobo la partida!" });
//         return;
//     }

//     // partida nova
//     const batalla = new Batalla({
//         id_ganador: req.body.id_ganador,
//         id_perdedor: req.body.id_perdedor
//     });

//     // el guardem a la bdd

//     batalla.save(batalla)
//         .then(data => {
//             res.send(data);
//         })
//         .catch(err => {
//             res.status(500).send({
//                 error: err.message || "Alguna cosa falla."
//             });
//         });
// });

// // consulta robots por ESTADISTICAS
// app.get("/api/estadisticas", (req, res) => {
//     Robot.find()
//         .then(data => {
//             // hacemos cosas con los datos antes de enviar...
//             const temp = [...data];
//             temp.sort((a, b) => (a[a.experiencia.victorias] > b[b.experiencia.victorias]) ? 1 : -1);
//             res.status(200).json(temp);
//         })
//         .catch(err => {
//             res.status(500).send({
//                 error: err.message || "Alguna cosa falla."
//             });
//         });
// });